<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../styles/style.css">
    <title>Таск-трекер - Работники</title>
  </head>
  <body>
    <?php
      include '../backend/get_data.php';
    ?>
    <header class="header">
      <div class="header__container">
        <p class="header__title">Таск-трекер</p>
        <form method="get" class="header__form-search form-search">
          <input class="form-search__input place_for_search" type="text" id="text-to-find" value="" placeholder="Найти..." autofocus>
          <input class="form-search__input form-search__input-btn button_for_search" type="submit" onclick="javascript: FindOnPage('text-to-find',true); return false;" value="Поиск" title="Поиск">
        </form>
        <nav class="header__nav nav">
          <ul class="nav__list">
            <li class="nav__item"><a href="../index.html" class="nav__link">На главную</a></li>
            <li class="nav__item"><a href="projects.php" class="nav__link">Проекты</a></li>
            <li class="nav__item"><a href="tasks.php" class="nav__link">Задания</a></li>
            <li class="nav__item"><a href="workers.php" class="nav__link">Работники</a></li>
          </ul>
        </nav>
      </div>
    </header>

    <main class="main">
      <section class="main-section">
        <div class="main-section__container">
          <h1 class="main-section__title">Работники</h1>
          <div class="main-sec__workers-column workers-column">
            <p class="workers-column__title">Руководители/менеджеры</p>
            <ul class="workers-column__list workers-column__list-bg-blue">
              <?php
              $RW = getRW();
              while($row = $RW->fetch_assoc()) {
                if ($row['r_id'] == 2) {
                  $workerName = sql_query('SELECT w_name FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_name'];
                  $workerPost = sql_query('SELECT w_post FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_post'];
                  $workerNumber = sql_query('SELECT w_number FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_number'];
                ?>
                <li class="projects-column__item">
                  <?php echo $workerName, '<br>', ' Должность: ', $workerPost, '<br>', ' Номер: ', $workerNumber ?>
                </li>
                <?php
                }
              }
            ?>
            </ul>
          </div>
          <div class="main-sec__workers-column workers-column">
            <p class="workers-column__title">Работники</p>
            <ul class="workers-column__list workers-column__list-bg-blue">
              <?php
              $RW = getRW();
              while($row = $RW->fetch_assoc()) {
                if ($row['r_id'] == 1) {
                  $workerName = sql_query('SELECT w_name FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_name'];
                  $workerPost = sql_query('SELECT w_post FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_post'];
                  $workerNumber = sql_query('SELECT w_number FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_number'];
                ?>
                <li class="projects-column__item">
                  <?php echo $workerName, '<br>', ' Должность: ', $workerPost, '<br>', ' Номер: ', $workerNumber ?>
                </li>
                <?php
                }
              }
            ?>
            </ul>
          </div>
          <div class="main-sec__workers-column workers-column">
            <p class="workers-column__title">Админы</p>
            <ul class="workers-column__list workers-column__list-bg-blue">
              <?php
              $RW = getRW();
              while($row = $RW->fetch_assoc()) {
                if ($row['r_id'] == 3) {
                  $workerName = sql_query('SELECT w_name FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_name'];
                  $workerPost = sql_query('SELECT w_post FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_post'];
                  $workerNumber = sql_query('SELECT w_number FROM `std_1720_task_tracker`.`workers` WHERE `w_id` =' . $row['w_id'])->fetch_assoc()['w_number'];
                ?>
                <li class="projects-column__item">
                  <?php echo $workerName, '<br>', ' Должность: ', $workerPost, '<br>', ' Номер: ', $workerNumber ?>
                </li>
                <?php
                }
              }
            ?>
            </ul>
          </div>

          <form action="../backend/form-workers.php" method="post" class="main-section__form form">
            <p class="form__title">Добавить работника</p>
            <label for="w_name" class="form__label">Имя работника</label>
            <input type="text" class="form__input" id="w_name" name="w_name">
            <label for="w_post" class="form__label">Должность работника</label>
            <input type="text" class="form__input" id="w_post" name="w_post">
            <label for="w_number" class="form__label">Номер работника</label>
            <input type="text" class="form__input" id="w_number" name="w_number">
            <select name="r_name" id="r_name" class="form__input form__input-select">
              <option value="Работник" class="form__option">Работник</option>
              <option value="Руководитель/менеджер" class="form__option">Руководитель/менеджер</option>
              <option value="Админ" class="form__option">Админ</option>
            </select>
            <input class="form__input form__input-btn" type="submit" name="submit" value="Добавить">
          </form>

        </div>
      </section>
    </main>
  </body>
  <script type="text/javascript">
    var lastResFind="";
    var copy_page="";
    function TrimStr(s) {
       s = s.replace( /^\s+/g, '');
    return s.replace( /\s+$/g, '');
    }
    function FindOnPage(inputId) {
    var obj = window.document.getElementById(inputId);
    var textToFind;

    if (obj) {
      textToFind = TrimStr(obj.value);
    } else {
      return;
    }
    if (textToFind == "") {
      return;
    }

    if(document.body.innerHTML.indexOf(textToFind)=="-1")

    if(copy_page.length>0)
          document.body.innerHTML=copy_page;
    else copy_page=document.body.innerHTML;


    document.body.innerHTML = document.body.innerHTML.replace(eval("/name="+lastResFind+"/gi")," ");
    document.body.innerHTML = document.body.innerHTML.replace(eval("/"+textToFind+"/gi"),"<a name="+textToFind+" style='background:red'>"+textToFind+"</a>");
    lastResFind=textToFind;
    window.location = '#'+textToFind;
    }
  </script>
</html>

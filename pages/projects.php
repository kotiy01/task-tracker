<!DOCTYPE html>
<html lang="ru" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../styles/style.css">
    <title>Таск-трекер - Проекты</title>
  </head>
  <body>
    <?php
      include '../backend/get_data.php';
    ?>
    <header class="header">
      <div class="header__container">
        <p class="header__title">Таск-трекер</p>
        <form method="get" class="header__form-search form-search">
          <input class="form-search__input place_for_search" type="text" id="text-to-find" value="" placeholder="Найти..." autofocus>
          <input class="form-search__input form-search__input-btn button_for_search" type="submit" onclick="javascript: FindOnPage('text-to-find',true); return false;" value="Поиск" title="Поиск">
        </form>
        <nav class="header__nav nav">
          <ul class="nav__list">
            <li class="nav__item"><a href="../index.html" class="nav__link">На главную</a></li>
            <li class="nav__item"><a href="projects.php" class="nav__link">Проекты</a></li>
            <li class="nav__item"><a href="tasks.php" class="nav__link">Задания</a></li>
            <li class="nav__item"><a href="workers.php" class="nav__link">Работники</a></li>
          </ul>
        </nav>
      </div>
    </header>

    <main class="main">
      <section class="main-section">
        <div class="main-section__container">
          <h1 class="main-section__title">Проекты</h1>
          <div class="main-section__projects-column projects-column">
            <p class="projects-column__title">В работе</p>
            <ul class="projects-column__list projects-column__list-bg-green">
              <?php
                $SP = getSP();
                while($row = $SP->fetch_assoc()) {
                  if ($row['s_id'] == 2) {
                    $projectName = sql_query('SELECT p_name FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_name'];
                    $projectDStart = sql_query('SELECT p_date_start FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_date_start'];
                    $projectDEnd = sql_query('SELECT p_date_end FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_date_end'];
                  ?>
                  <li class="projects-column__item">
                    <?php echo $projectName, '<br>', ' Дата начала работы: ', $projectDStart, '<br>', ' Дата окончания работы: ', $projectDEnd ?>
                  </li>
                  <?php
                  }
                }
              ?>
            </ul>
          </div>
          <div class="main-section__projects-column projects-column">
            <p class="projects-column__title">Отложенные</p>
            <ul class="projects-column__list projects-column__list-bg-orng">
              <?php
                $SP = getSP();
                while($row = $SP->fetch_assoc()) {
                  if ($row['s_id'] == 3) {
                    $projectName = sql_query('SELECT p_name FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_name'];
                    $projectDStart = sql_query('SELECT p_date_start FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_date_start'];
                    $projectDEnd = sql_query('SELECT p_date_end FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_date_end'];
                  ?>
                  <li class="projects-column__item">
                    <?php echo $projectName, '<br>', ' Дата начала работы: ', $projectDStart, '<br>', ' Дата окончания работы: ', $projectDEnd ?>
                  </li>
                  <?php
                  }
                }
              ?>
            </ul>
          </div>
          <div class="main-section__projects-column projects-column">
            <p class="projects-column__title">Завершенные</p>
            <ul class="projects-column__list projects-column__list-bg-red">
              <?php
                $SP = getSP();
                while($row = $SP->fetch_assoc()) {
                  if ($row['s_id'] == 1) {
                    $projectName = sql_query('SELECT p_name FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_name'];
                    $projectDStart = sql_query('SELECT p_date_start FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_date_start'];
                    $projectDEnd = sql_query('SELECT p_date_end FROM `std_1720_task_tracker`.`projects` WHERE `p_id` =' . $row['p_id'])->fetch_assoc()['p_date_end'];
                  ?>
                  <li class="projects-column__item">
                    <?php echo $projectName, '<br>', ' Дата начала работы: ', $projectDStart, '<br>', ' Дата окончания работы: ', $projectDEnd ?>
                  </li>
                  <?php
                  }
                }
              ?>
            </ul>
          </div>

          <form action="../backend/form-projects.php" method="post" class="main-section__form form">
            <p class="form__title">Добавить проект</p>
            <label for="p_name" class="form__label">Название проекта</label>
            <input type="text" class="form__input" id="p_name" name="p_name">
            <label for="p_date_start" class="form__label">Дата начала работы</label>
            <input type="date" class="form__input" id="p_date_start" name="p_date_start">
            <label for="p_date_end" class="form__label">Дата окончания работы</label>
            <input type="date" class="form__input" id="p_date_end" name="p_date_end">
            <select name="s_name" id="s_name" class="form__input form__input-select">
              <option value="Завершенные" class="form__option">Завершенные</option>
              <option value="В работе" class="form__option">В работе</option>
              <option value="Отложенные" class="form__option">Отложенные</option>
            </select>
            <input class="form__input form__input-btn" type="submit" name="submit" value="Добавить">
          </form>

        </div>
      </section>
    </main>
  </body>
  <script type="text/javascript">
    var lastResFind="";
    var copy_page="";
    function TrimStr(s) {
       s = s.replace( /^\s+/g, '');
    return s.replace( /\s+$/g, '');
    }
    function FindOnPage(inputId) {
    var obj = window.document.getElementById(inputId);
    var textToFind;

    if (obj) {
      textToFind = TrimStr(obj.value);
    } else {
      return;
    }
    if (textToFind == "") {
      return;
    }

    if(document.body.innerHTML.indexOf(textToFind)=="-1")

    if(copy_page.length>0)
          document.body.innerHTML=copy_page;
    else copy_page=document.body.innerHTML;


    document.body.innerHTML = document.body.innerHTML.replace(eval("/name="+lastResFind+"/gi")," ");
    document.body.innerHTML = document.body.innerHTML.replace(eval("/"+textToFind+"/gi"),"<a name="+textToFind+" style='background:red'>"+textToFind+"</a>");
    lastResFind=textToFind;
    window.location = '#'+textToFind;
    }
  </script>
</html>

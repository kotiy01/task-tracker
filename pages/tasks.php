<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../styles/style.css">
    <title>Таск-трекер - Задания</title>
  </head>
  <body>
    <?php
      include '../backend/get_data.php';
    ?>
    <header class="header">
      <div class="header__container">
        <p class="header__title">Таск-трекер</p>
        <form method="get" class="header__form-search form-search">
          <input class="form-search__input place_for_search" type="text" id="text-to-find" value="" placeholder="Найти..." autofocus>
          <input class="form-search__input form-search__input-btn button_for_search" type="submit" onclick="javascript: FindOnPage('text-to-find',true); return false;" value="Поиск" title="Поиск">
        </form>
        <nav class="header__nav nav">
          <ul class="nav__list">
            <li class="nav__item"><a href="../index.html" class="nav__link">На главную</a></li>
            <li class="nav__item"><a href="projects.php" class="nav__link">Проекты</a></li>
            <li class="nav__item"><a href="tasks.php" class="nav__link">Задания</a></li>
            <li class="nav__item"><a href="workers.php" class="nav__link">Работники</a></li>
          </ul>
        </nav>
      </div>
    </header>

    <main class="main">
      <section class="main-section">
        <div class="main-section__container">
          <p class="main-section__title">Задания</p>
          <div class="main-section__tasks-column tasks-column">
            <p class="tasks-column__title">В работе</p>
            <ul class="tasks-column__list tasks-column__list-bg-green">
              <?php
                $ST = getST();
                while($row = $ST->fetch_assoc()) {
                  if ($row['s_id'] == 2) {
                    $taskDesc = sql_query('SELECT t_description FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_description'];
                    $taskDStart = sql_query('SELECT t_date_start FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_date_start'];
                    $taskDEnd = sql_query('SELECT t_date_end FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_date_end'];
                    $taskWorker = sql_query('SELECT t_worker FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_worker'];
                  ?>
                  <li class="tasks-column__item">
                    <?php echo $taskDesc, '<br>', ' Дата начала работы: ', $taskDStart, '<br>', ' Дата окончания работы: ', $taskDEnd, '<br>', ' Исполнитель: ', $taskWorker ?>
                  </li>
                  <?php
                  }
                }
              ?>
            </ul>
          </div>
          <div class="main-section__tasks-column tasks-column">
            <p class="tasks-column__title">Отложенные</p>
            <ul class="tasks-column__list tasks-column__list-bg-orng">
              <?php
                $ST = getST();
                while($row = $ST->fetch_assoc()) {
                  if ($row['s_id'] == 3) {
                    $taskDesc = sql_query('SELECT t_description FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_description'];
                    $taskDStart = sql_query('SELECT t_date_start FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_date_start'];
                    $taskDEnd = sql_query('SELECT t_date_end FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_date_end'];
                    $taskWorker = sql_query('SELECT t_worker FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_worker'];
                  ?>
                  <li class="tasks-column__item">
                    <?php echo $taskDesc, '<br>', ' Дата начала работы: ', $taskDStart, '<br>', ' Дата окончания работы: ', $taskDEnd, '<br>', ' Исполнитель: ', $taskWorker ?>
                  </li>
                  <?php
                  }
                }
              ?>
            </ul>
          </div>
          <div class="main-section__tasks-column tasks-column">
            <p class="tasks-column__title">Завершенные</p>
            <ul class="tasks-column__list tasks-column__list-bg-red">
              <?php
                $ST = getST();
                while($row = $ST->fetch_assoc()) {
                  if ($row['s_id'] == 1) {
                    $taskDesc = sql_query('SELECT t_description FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_description'];
                    $taskDStart = sql_query('SELECT t_date_start FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_date_start'];
                    $taskDEnd = sql_query('SELECT t_date_end FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_date_end'];
                    $taskWorker = sql_query('SELECT t_worker FROM `std_1720_task_tracker`.`tasks` WHERE `t_id` =' . $row['t_id'])->fetch_assoc()['t_worker'];
                  ?>
                  <li class="tasks-column__item">
                    <?php echo $taskDesc, '<br>', ' Дата начала работы: ', $taskDStart, '<br>', ' Дата окончания работы: ', $taskDEnd, '<br>', ' Исполнитель: ', $taskWorker ?>
                  </li>
                  <?php
                  }
                }
              ?>
            </ul>
          </div>

          <form action="../backend/form-tasks.php" method="post" class="main-section__form form">
            <p class="form__title">Добавить задание</p>
            <label for="t_description" class="form__label">Описание задания</label>
            <input type="text" class="form__input" id="t_description" name="t_description">
            <label for="t_date_start" class="form__label">Дата начала выполнения</label>
            <input type="date" class="form__input" id="t_date_start" name="t_date_start">
            <label for="t_date_end" class="form__label">Дата окончания выполнения</label>
            <input type="date" class="form__input" id="t_date_end" name="t_date_end">
            <label for="t_worker" class="form__label">Исполнитель</label>
            <input type="text" class="form__input" id="t_worker" name="t_worker">
            <select name="st_name" id="st_name" class="form__input form__input-select">
              <option value="Завершенные" class="form__option">Завершенные</option>
              <option value="В работе" class="form__option">В работе</option>
              <option value="Отложенные" class="form__option">Отложенные</option>
            </select>
            <input class="form__input form__input-btn" type="submit" name="submit" value="Добавить">
          </form>

        </div>
      </section>
    </main>
  </body>
  <script type="text/javascript">
    var lastResFind="";
    var copy_page="";
    function TrimStr(s) {
       s = s.replace( /^\s+/g, '');
    return s.replace( /\s+$/g, '');
    }
    function FindOnPage(inputId) {
    var obj = window.document.getElementById(inputId);
    var textToFind;

    if (obj) {
      textToFind = TrimStr(obj.value);
    } else {
      return;
    }
    if (textToFind == "") {
      return;
    }

    if(document.body.innerHTML.indexOf(textToFind)=="-1")

    if(copy_page.length>0)
          document.body.innerHTML=copy_page;
    else copy_page=document.body.innerHTML;


    document.body.innerHTML = document.body.innerHTML.replace(eval("/name="+lastResFind+"/gi")," ");
    document.body.innerHTML = document.body.innerHTML.replace(eval("/"+textToFind+"/gi"),"<a name="+textToFind+" style='background:red'>"+textToFind+"</a>");
    lastResFind=textToFind;
    window.location = '#'+textToFind;
    }
  </script>
</html>

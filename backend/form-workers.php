<?php
  session_start();
  include_once 'connect_db.php';
  include 'get_data.php';

  $workers = $_POST['w_name'];
  $workersPost = $_POST['w_post'];
  $workersNumber = $_POST['w_number'];
  $roles = $_POST['r_name'];

  if (sql_query('SELECT * FROM workers WHERE w_name =' . add_quotes($workers))->num_rows == 0) {
    sql_query('INSERT INTO workers (`w_id`,`w_name`,`w_post`,`w_number`) VALUES (NULL, ' . add_quotes($workers) . ', ' . add_quotes($workersPost) . ', ' . add_quotes($workersNumber) . ')');
    $roleID = sql_query('SELECT * FROM roles WHERE r_name =' . add_quotes($roles))->fetch_assoc()['r_id'];
    $workerID = sql_query('SELECT * FROM workers WHERE w_name =' . add_quotes($workers))->fetch_assoc()['w_id'];
    sql_query('INSERT INTO rw (`r_id`,`w_id`) VALUES (' . add_quotes($roleID) . ', ' . add_quotes($workerID) . ')');
  }

  header('Location: ../pages/workers.php');
?>

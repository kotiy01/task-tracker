<?php
include_once 'connect_db.php';

function getProjects() {
    $db = sql_query('SELECT * FROM projects');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row[''];
    }
    return $res;
}

function getStatuses() {
    $db = sql_query('SELECT * FROM statuses');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row[''];
    }
    return $res;
}

function getSP() {
    $res = sql_query('SELECT * FROM sp');
    return $res;
}



function getWorkers() {
    $db = sql_query('SELECT * FROM workers');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row[''];
    }
    return $res;
}

function getRoles() {
    $db = sql_query('SELECT * FROM roles');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row[''];
    }
    return $res;
}

function getRW() {
    $res = sql_query('SELECT * FROM rw');
    return $res;
}



function getTasks() {
    $db = sql_query('SELECT * FROM tasks');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row[''];
    }
    return $res;
}

function getStatus() {
    $db = sql_query('SELECT * FROM status');
    $res = [];
    while ($row = $db->fetch_assoc()) {
        $res[] = $row[''];
    }
    return $res;
}

function getST() {
    $res = sql_query('SELECT * FROM st');
    return $res;
}
?>

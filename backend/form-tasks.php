<?php
  session_start();
  include_once 'connect_db.php';
  include 'get_data_tasks.php';

  $tasks = $_POST['t_description'];
  $taskDStart = $_POST['t_date_start'];
  $taskDEnd = $_POST['t_date_end'];
  $taskWorker = $_POST['t_worker'];
  $statuses = $_POST['st_name'];

  if (sql_query('SELECT * FROM tasks WHERE t_description =' . add_quotes($tasks))->num_rows == 0) {
    sql_query('INSERT INTO tasks (`t_id`,`t_description`,`t_date_start`,`t_date_end`,`t_worker`) VALUES (NULL, ' . add_quotes($tasks) . ', ' . add_quotes($taskDStart) . ', ' . add_quotes($taskDEnd) . ', ' . add_quotes($taskWorker) . ')');
    $statusID = sql_query('SELECT * FROM status WHERE st_name =' . add_quotes($statuses))->fetch_assoc()['st_id'];
    $taskID = sql_query('SELECT * FROM tasks WHERE t_description =' . add_quotes($tasks))->fetch_assoc()['t_id'];
    sql_query('INSERT INTO st (`s_id`,`t_id`) VALUES (' . add_quotes($statusID) . ', ' . add_quotes($taskID) . ')');
  }

  header('Location: ../pages/tasks.php');
?>

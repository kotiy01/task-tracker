<?php
  session_start();
  include_once 'connect_db.php';
  include 'get_data.php';

  $projects = $_POST['p_name'];
  $projectsDStart = $_POST['p_date_start'];
  $projectsDEnd = $_POST['p_date_end'];
  $statuses = $_POST['s_name'];

  if (sql_query('SELECT * FROM projects WHERE p_name =' . add_quotes($projects))->num_rows == 0) {
    sql_query('INSERT INTO projects (`p_id`,`p_name`,`p_date_start`,`p_date_end`) VALUES (NULL, ' . add_quotes($projects) . ', ' . add_quotes($projectsDStart) . ', ' . add_quotes($projectsDEnd) . ')');
    $statusID = sql_query('SELECT * FROM statuses WHERE s_name =' . add_quotes($statuses))->fetch_assoc()['s_id'];
    $projectID = sql_query('SELECT * FROM projects WHERE p_name =' . add_quotes($projects))->fetch_assoc()['p_id'];
    sql_query('INSERT INTO sp (`s_id`,`p_id`) VALUES (' . add_quotes($statusID) . ', ' . add_quotes($projectID) . ')');
  }

  header('Location: ../pages/projects.php');
?>
